FROM centos:latest
# copy inside files in dockerfile
COPY /MQ /opt/
COPY config.mqsc /etc/mqm/
COPY webspheremq.service  /etc/systemd/system/

RUN yum -y update;\ 
    (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
    rm -f /lib/systemd/system/multi-user.target.wants/*;\
    rm -f /etc/systemd/system/*.wants/*;\
    rm -f /lib/systemd/system/local-fs.target.wants/*; \
    rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
    rm -f /lib/sysclstemd/system/basic.target.wants/*;\
    rm -f /lib/systemd/system/anaconda.target.wants/*


RUN mkdir -p /tmp/mq_license_8.0.0/license/ \
    && echo -e "#rava\nstatus=9\n"  > /tmp/mq_license_8.0.0/license/status.dat \
    && cd /opt/ \
    && rpm -ivh MQ*.rpm \
    && rm /opt/*.rpm \
    && chown -R mqm:mqm /opt/mqm/ \
    && chmod +x /opt/mqm/bin/ \
    && /opt/mqm/bin/setmqinst -p /opt/mqm/ -i \
    &&  su mqm -c  "crtmqm QM  && strmqm QM" \
    && echo -e "SSL:\n   AllowWeakCipherSpec=TLS_RSA_WITH_NULL_SHA256" >> /var/mqm/qmgrs/QM/qm.ini \
    && su mqm -c "runmqsc QM  < /etc/mqm/config.mqsc" \
    && systemctl enable /etc/systemd/system/webspheremq.service
VOLUME ["/sys/fs/cgroup"]
EXPOSE 1414
ENV LANG=en_US.UTF-8
CMD ["/usr/sbin/init"]
