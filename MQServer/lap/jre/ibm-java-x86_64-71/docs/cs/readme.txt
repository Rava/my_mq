Sada IBM SDK for Linux, Java Technology Edition, verze 7, vydání 1
==================================================================

Tento soubor README se vztahuje k verzi 7, vydání 1, a ke všem dalším
úpravám a servisním aktualizacím, pokud nebude v novém souboru
README stanoveno jinak.

Společnost IBM poskytuje se sadou SDK další obsah.

Dokumentace k produktu:
-----------------------

Uživatelské příručky a příručky zabezpečení podporující toto vydání jsou
k dispozici pro prohlížení online:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Tyto příručky lze také stáhnout z webu IBM developerWorks:
https://www.ibm.com/developerworks/java/jdk/docs.html.

Nejnovější informace jsou uvedeny v následující technické poznámce
společnosti IBM:
http://www.ibm.com/support/docview.wss?uid=swg21639279