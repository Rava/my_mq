IBM SDK for Linux, Java Technology Edition, Version 7 Release 1
===============================================================

Diese Readme-Datei bezieht sich auf Version 7 Release 1 und alle nachfolgenden
Modifikationen und Serviceaktualisierungen, bis dieser Hinweise in einer neuen
Readme-Datei geändert wird.

IBM stellt mit dem SDK Dokumentation bereit.

Produktdokumentation:
---------------------

Unterstützende Benutzer- und Sicherheitshandbücher für dieses Release können
online angezeigt werden:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Diese Handbücher können auch von IBM developerWorks heruntergeladen werden:
https://www.ibm.com/developerworks/java/jdk/docs.html

Die aktuellen News finden Sie im folgenden technischen Hinweis des IBM Support:
http://www.ibm.com/support/docview.wss?uid=swg21639279