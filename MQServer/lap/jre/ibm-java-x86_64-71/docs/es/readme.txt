IBM SDK para Linux, Java Technology Edition, Versión 7 Release 1
===============================================================

Este archivo LÉAME se aplica a la versión 7 release 1 y a todas las modificaciones y actualizaciones de servicio posteriores, hasta que se indique lo contrario en un nuevo archivo LÉAME. 

IBM proporciona contenido adicional con el SDK.

Documentación del producto:
----------------------

Están disponibles guías de usuario y guías de seguridad para dar soporte a este release para su visualización en línea:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Estas guías también están disponibles para su descarga desde IBM developerWorks:
https://www.ibm.com/developerworks/java/jdk/docs.html

Las noticias de última hora pueden encontrarse en la siguiente nota técnica de soporte de
IBM: http://www.ibm.com/support/docview.wss?uid=swg21639279