IBM SDK per a Linux, Java Technology Edition, Versió 7 Release 1
===============================================================

El fitxer README s'aplica a la versió 7 release 1 i a totes les modificacions i actualitzacions de servei posteriors, fins que s'indiqui el contrari a un nou fitxer README. 

IBM proporciona continguts addicionals amb el SDK.

Documentació del producte:
--------------------------

Podeu trobar guies d'usuari i guies de seguretat per donar suport a aquest release per visualitzar-les en línia:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Aquestes guies també estan disponibles per baixar-es des d'IBM developerWorks:
https://www.ibm.com/developerworks/java/jdk/docs.html

Les notícies més recents es poden trobar a la següent nota tècnica de suport d'IBM:
http://www.ibm.com/support/docview.wss?uid=swg21639279