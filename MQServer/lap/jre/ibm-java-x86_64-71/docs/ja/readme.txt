IBM SDK for Linux, Java Technology Edition バージョン 7 リリース 1
==================================================================

この README ファイルは、バージョン 7 リリース 1、および新しい README ファイルで
明記されていない限り、以降のすべてのモディフィケーションおよび
Service Refresh に適用されます。

IBM では、SDK に関する以下のコンテンツを提供しています。

製品資料:
---------

このリリースをサポートするユーザー・ガイドおよびセキュリティー・ガイドは、
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp でオンラインで
ご利用いただけます。

これらのガイドは、IBM developerWorks (https://www.ibm.com/developerworks/java/jdk/docs.html)
からダウンロードしてご利用いただくこともできます。

製品の最新情報については、次の IBM サポート技術情報にアクセスしてください:
http://www.ibm.com/support/docview.wss?uid=swg21639279