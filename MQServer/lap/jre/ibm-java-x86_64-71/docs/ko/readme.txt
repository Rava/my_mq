Linux용 IBM SDK, Java Technology Edition, 버전 7 릴리스 1
=========================================================

이 README 파일은 새 README 파일에서 별도로 명시하지 않는 한
버전 7 릴리스 1 및 모든 후속 수정사항과 서비스 갱신사항에 적용됩니다.

IBM은 SDK와 추가 컨텐츠를 제공합니다.

제품 문서:
----------------------

다음 컴포넌트를 지원하는 제품 문서는 IBM developerWorks에 있습니다.

- 보안
- API 문서
- ORB 안내서
- RMI-IIOP 프로그래머 안내서

https://www.ibm.com/developerworks/java/jdk/docs.html을 참조하십시오.

최신 뉴스는 다음 IBM 지원 센터 기술 노트에서 볼 수 있습니다.
http://www.ibm.com/support/docview.wss?uid=swg21499721