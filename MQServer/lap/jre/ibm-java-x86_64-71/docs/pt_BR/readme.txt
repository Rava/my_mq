IBM SDK para Linux, Java Technology Edition, Versão 7 Liberação 1
===============================================================

Este arquivo LEIA-ME se aplica à Versão 7 Liberação 1, e a todas as modificações e
atualizações de serviço subsequentes, até que seja indicado de outro modo em um novo
arquivo LEIA-ME.

A IBM fornece conteúdo adicional com o SDK.

Documentação do produto:
------------------------

Os guias de usuário e de segurança que dão suporte a esta liberação estão disponíveis
para visualização online: http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Esses guias também estão disponíveis para download no IBM developerWorks: https://www.ibm.com/developerworks/java/jdk/docs.html

Quaisquer novidades posteriores podem ser localizadas na seguinte nota técnica do suporte IBM:
http://www.ibm.com/support/docview.wss?uid=swg21639279