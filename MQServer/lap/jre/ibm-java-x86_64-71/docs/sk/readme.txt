IBM SDK for Linux, Java Technology Edition, verzia 7, vydanie 1
===============================================================

Tento súbor README je určený pre verziu 7, vydanie 1 a všetky následné
modifikácie a obnovy služby, ak v novom súbore README nie je uvedené inak.

Spoločnosť IBM poskytuje ďalší obsah so SDK.

Dokumentácia k produktu:
------------------------

Užívateľské príručky a sprievodcovia zabezpečením pre toto vydanie sú k
dispozícii na prezeranie online:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Tieto príručky sú tiež k dispozícii na prevzatie z lokality IBM developerWorks:
https://www.ibm.com/developerworks/java/jdk/docs.html

Najnovšie novinky nájdete v tejto technickej poznámke IBM:
http://www.ibm.com/support/docview.wss?uid=swg21639279