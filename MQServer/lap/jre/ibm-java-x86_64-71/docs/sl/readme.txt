IBM SDK for Linux, Java Technology Edition, 1. izdaja različice 7
===============================================================

Ta datoteka README se nanaša na 1. izdajo različice 7 in na vse
nadaljnje spremembe in osvežitve storitve, dokler ni v novi
datoteki README navedeno drugače.

IBM s SDK-jem nudi dodatno vsebino.

Dokumentacija izdelka:
----------------------

Uporabniški priročniki in priročniki za zaščito za podporo te izdaje so na voljo za spletni ogled:
http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

Te vodnike lahko prenesete tudi s spletnega mesta IBM developerWorks:
https://www.ibm.com/developerworks/java/jdk/docs.html

Zadnje novice lahko najdete v naslednji tehnični opombi za podporo IBM:
http://www.ibm.com/support/docview.wss?uid=swg21639279