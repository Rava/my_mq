IBM SDK for Linux Java Technology Edition Version 7 Release 1
===============================================================

本自述文件适用于 Version 7 Release 1 和所有后续修订包和服务更新，除非新的自述文件中另有声明。

IBM 还通过 SDK 提供了其他内容。 

产品文档：
----------------------

在以下站点可联机查看支持本发行版的用户指南和安全指南：http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp

也可从位于以下站点的 IBM developerWorks 下载这些指南：https://www.ibm.com/developerworks/java/jdk/docs.html

在以下 IBM 支持技术说明站点上可找到最近的所有新闻：http://www.ibm.com/support/docview.wss?uid=swg21639279