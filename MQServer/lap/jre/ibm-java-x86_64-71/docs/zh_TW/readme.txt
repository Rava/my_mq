IBM SDK for Linux, Java Technology Edition Version 7 Release 1 版
=================================================================

本 Readme 檔適用於 Version 7 Release 1 版，除非新的 Readme 檔另有規定，否則亦適用於所有後續版本、修改及服務更新。

IBM 提供了有關 SDK 的其他內容。

產品說明文件：
----------------------

IBM 資訊中心提供了支援 IBM SDK for Linux, Java Technology Edition Version 7 Release 1 的使用手冊及安全手冊，以供線上檢視，網址為：http://publib.boulder.ibm.com/infocenter/java7sdk/v7r0/index.jsp。

您也可以從 IBM developerWorks 下載這些手冊，網址為：https://www.ibm.com/developerworks/java/jdk/docs.html。

您可以在下列 IBM 支援中心 Technote 中找到所有最新新聞：http://www.ibm.com/support/docview.wss?uid=swg21639279