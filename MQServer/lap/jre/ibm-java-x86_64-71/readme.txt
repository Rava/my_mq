﻿IBM SDK for Linux, Java Technology Edition, Version 7 Release 1
===============================================================

This README file applies to Version 7 Release 1, and to all
subsequent modifications and service refreshes, until otherwise
indicated in a new README file.

IBM provides additional content with the SDK.

Product documentation:
----------------------

User guides and security guides to support this release are available for online viewing:
https://www.ibm.com/support/knowledgecenter/SSYKE2_7.0.0/welcome/welcome_javasdk_version71.html

These guides are also available for download from IBM developerWorks: 
https://www.ibm.com/developerworks/java/jdk/docs.html

Any late breaking news can be found in the following IBM support technote:
http://www.ibm.com/support/docview.wss?uid=swg21639279