Summary: WebSphere MQ Explorer
Name: MQSeriesExplorer
Version: 8.0.0
Release: 4
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesRuntime = 8.0.0-4
Requires: MQSeriesJRE = 8.0.0-4
%define _source_filedigest_algorithm md5
%define _binary_filedigest_algorithm md5
%define _source_payload w7.lzdio
%define _binary_payload w7.lzdio
%global __strip /bin/true
%global _rpmdir /build/slot1/p800_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p800_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p800_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72 
This package provides the WebSphere MQ Explorer capability, which is a
graphical tool used to administer and monitor WebSphere MQ queue managers.
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/swt
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/rootpage
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/elcl16
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/links
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/dropins
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer-8.0.0.cmptag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer-8.0.0.cmptag
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/bin/strmqcfg $RPM_BUILD_ROOT/opt/mqm/bin/strmqcfg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/bin/MQExplorer $RPM_BUILD_ROOT/opt/mqm/bin/MQExplorer
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/bin/MQExplorer.ini $RPM_BUILD_ROOT/opt/mqm/bin/MQExplorer.ini
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/config.ini $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/config.ini
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_8.0.0.201510221435 $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_8.0.0.201510221435
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product.root.feature_root_8.0.0.201510221436 $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product.root.feature_root_8.0.0.201510221436
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_8.0.0.201510221436 $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_8.0.0.201510221436
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131138.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131138.profile.gz
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131502.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131502.profile.gz
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521148184.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521148184.profile.gz
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521146694.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521146694.profile.gz
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/.eclipseproduct $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/.eclipseproduct
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_fr.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_fr.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ru.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ru.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_id.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_id.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_gr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_gr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_tr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_tr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_CN.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_CN.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_es.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_es.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_si.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_si.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ja_JP.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ja_JP.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_TW.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_TW.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_it.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_it.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pt_BR.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pt_BR.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_hu.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_hu.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_CN.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_CN.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pl.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pl.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ko.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ko.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/category.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/category.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_de.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_de.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ja_JP.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ja_JP.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_cs.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_cs.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_lt.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_lt.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/eclipse.inf
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_id.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_id.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_gr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_gr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_tr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_tr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_CN.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_CN.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_si.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_si.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ja_JP.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ja_JP.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_lt.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_lt.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/eclipse.inf
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/eclipse_update_120.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/eclipse_update_120.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_hu.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_ru.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_pl.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_cs.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ja.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_fr.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_es.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_HK.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_it.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_de.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_pt_BR.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ko.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_TW.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/eclipse.inf
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/eclipse_update_120.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/eclipse_update_120.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/license.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/epl-v10.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/epl-v10.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl1_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl1_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl2_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl2_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.4.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.4.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl2_3.7.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl2_3.7.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.2.0.v20130828-0031.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.2.0.v20130828-0031.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.4.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.4.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl1_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl1_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui_3.7.1.v20130729-1104.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui_3.7.1.v20130729-1104.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.command_0.10.0.v201209301215.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.command_0.10.0.v201209301215.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user_4.3.0.v20130605-1059.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user_4.3.0.v20130605-1059.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.301.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.301.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.100.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.100.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/launcher.gtk.linux.x86_64.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/launcher.gtk.linux.x86_64.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/eclipse_1506.so $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/eclipse_1506.so
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.102.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.102.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclientprereqs_8.0.0.4.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclientprereqs_8.0.0.4.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util.gui_1.6.0.v201011041432.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util.gui_1.6.0.v201011041432.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_5.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_5.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.shell_0.10.0.v201212101605.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.shell_0.10.0.v201212101605.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.500.v20130404-1337.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.500.v20130404-1337.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.6.100.v20130326-1250.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.6.100.v20130326-1250.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util_3.2.300.v20130513-1956.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util_3.2.300.v20130513-1956.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_50.1.1.v201304230130.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_50.1.1.v201304230130.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.200.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.200.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.io_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.io_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl2_0.11.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl2_0.11.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl1_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl1_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.102.1.v20130827-2021.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.102.1.v20130827-2021.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.200.v20130419-1850.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.200.v20130419-1850.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.4.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.4.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.9.1.v20130704-1828.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.9.1.v20130704-1828.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di_1.0.0.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di_1.0.0.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.2.0.v20130424-1801.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.2.0.v20130424-1801.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.css.sac_1.3.1.v200903091627.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.css.sac_1.3.1.v200903091627.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets_3.4.200.v20130326-1254.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets_3.4.200.v20130326-1254.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.3.0.v20130509-0110.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.3.0.v20130509-0110.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl2_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl2_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi_2.9.1.v20130827-0309.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi_2.9.1.v20130827-0309.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.8.101.v20130802-1147.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.8.101.v20130802-1147.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.300.v20130517-0139.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.300.v20130517-0139.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl1_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl1_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl1_3.4.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl1_3.4.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl1_4.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl1_4.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.3.100.v20130513-1956.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.3.100.v20130513-1956.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.4.1.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.4.1.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl2_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl2_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt_0.12.1.v20130815-1438.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt_0.12.1.v20130815-1438.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl1_0.9.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl1_0.9.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.3.5.v201308161310.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.3.5.v201308161310.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl2_4.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl2_4.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl1_3.2.600.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl1_3.2.600.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl2_3.4.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl2_3.4.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl2_4.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl2_4.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser_3.4.100.v20130527-1656.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser_3.4.100.v20130527-1656.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl1_2.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl1_2.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.1.100.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.1.100.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl1_3.2.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl1_3.2.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl2_0.9.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl2_0.9.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_5.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_5.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl2_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl2_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl2_2.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl2_2.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.200.v20130326-1255.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.200.v20130326-1255.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.4.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.4.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl2_3.7.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl2_3.7.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di_1.3.0.v20130514-1256.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di_1.3.0.v20130514-1256.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common_2.9.1.v20130827-0309.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common_2.9.1.v20130827-0309.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl1_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl1_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.4.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.4.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpcore_4.1.4.v201203221030.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpcore_4.1.4.v201203221030.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.300.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.300.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.runtime_0.10.0.v201209301036.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.runtime_0.10.0.v201209301036.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl1_3.7.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl1_3.7.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.400.v20130418-1354.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.400.v20130418-1354.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl1_0.10.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl1_0.10.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.105.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.105.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_3.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_3.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl2_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl2_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl1_3.6.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl1_3.6.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl1_0.11.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl1_0.11.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.9.0.v20130516-1713.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.9.0.v20130516-1713.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl1_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl1_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.4.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.4.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl1_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl1_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools_1.0.200.v20130402-1741.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools_1.0.200.v20130402-1741.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.security_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.security_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl2_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl2_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl1_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl1_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.6.200.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.6.200.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.3.0.v20130412-2032.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.3.0.v20130412-2032.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services_1.0.1.v20130909-1436.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services_1.0.1.v20130909-1436.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts_1.3.1.v20130905-0905.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts_1.3.1.v20130905-0905.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.300.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.300.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl1_0.10.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl1_0.10.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport_1.1.0.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport_1.1.0.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.700.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.700.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources_3.4.500.v20130516-1049.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources_3.4.500.v20130516-1049.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.6.1.v20130822-1117.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.6.1.v20130822-1117.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl1_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl1_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.100.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.100.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme_0.9.100.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme_0.9.100.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl1_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl1_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl2_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl2_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl2_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl2_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.5.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.5.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl1_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl1_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools_3.2.200.v20130508-2007.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools_3.2.200.v20130508-2007.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl1_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl1_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpclient_4.1.3.v201209201135.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpclient_4.1.3.v201209201135.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.3.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.3.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl2_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl2_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.continuation_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.continuation_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl2_3.7.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl2_3.7.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions_0.11.100.v20130514-1256.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions_0.11.100.v20130514-1256.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.3.0.v20130526-2122.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.3.0.v20130526-2122.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite_1.0.400.v20130515-2028.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite_1.0.400.v20130515-2028.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.6.200.v20130514-1258.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.6.200.v20130514-1258.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.3.0.v20130526-0335.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.3.0.v20130526-0335.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.9.1.v20130814-1242.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.9.1.v20130814-1242.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl2_2.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl2_2.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.6.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.6.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.200.v20130430-1352.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.200.v20130430-1352.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change_2.9.0.v20130827-0309.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change_2.9.0.v20130827-0309.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.102.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.102.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.301.v20130717-1549.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.301.v20130717-1549.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl1_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl1_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.annotation_1.1.0.v201209060031.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.annotation_1.1.0.v201209060031.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.500.v20130402-1746.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.500.v20130402-1746.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.4.101.v20130813-1853.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.4.101.v20130813-1853.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclient_8.0.0.4.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclient_8.0.0.4.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.codec_1.4.0.v201209201156.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.codec_1.4.0.v201209201156.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.8.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.8.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed_3.6.0.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed_3.6.0.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl2_3.2.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl2_3.2.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl1_1.3.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl1_1.3.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net_1.2.200.v20120807-0927.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net_1.2.200.v20120807-0927.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper.glassfish_2.2.2.v201205150955.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper.glassfish_2.2.2.v201205150955.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.200.v20130326-1319.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.200.v20130326-1319.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.700.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.700.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl2_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl2_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/splash.bmp $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/splash.bmp
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.ini $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.ini
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/helpData.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/helpData.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro-eclipse.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro-eclipse.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse_lg.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse_lg.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/.api_description $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/.api_description
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/narrow_book.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/narrow_book.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/disabled_book.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/disabled_book.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.ini $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.ini
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/tutorialsExtensionContent.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/tutorialsExtensionContent.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/overviewExtensionContent.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/overviewExtensionContent.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent1.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent1.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent2.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent2.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent3.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent3.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_merge.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_merge.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_checkout.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_checkout.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/platform.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/platform.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/introData.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/introData.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_win7.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_win7.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_blu.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_blu.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_basestyle.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_basestyle.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_gtk.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_gtk.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_win7.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_win7.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_winxp.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_winxp.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mru_on_win7.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mru_on_win7.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_olv.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_olv.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mac.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mac.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkTSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkTSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/arrow.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/arrow.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macGrey.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macGrey.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/dragHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/dragHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkGrey.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkGrey.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7Handle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7Handle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPTSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPTSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macTSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macTSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPOlive.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPOlive.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluTSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluTSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicTSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicTSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBlue.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBlue.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macHandle.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macHandle.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7TSFrame.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7TSFrame.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/macosx_narrow_book.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/macosx_narrow_book.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/book.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/book.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.mappings $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.mappings
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/LegacyIDE.e4xmi $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/LegacyIDE.e4xmi
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse256.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse256.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl1_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl1_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.300.v20130503-1750.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.300.v20130503-1750.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl1_4.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl1_4.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl1_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl1_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.5.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.5.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/fragment.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/.api_description $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/.api_description
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/runtime_registry_compatibility.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/runtime_registry_compatibility.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl2_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl2_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl2_0.10.2.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl2_0.10.2.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search_3.9.0.v20130312-1625.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search_3.9.0.v20130312-1625.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.300.v20130515-1451.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.300.v20130515-1451.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.8.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.8.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl1_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl1_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.el_2.2.0.v201303151357.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.el_2.2.0.v201303151357.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets_1.0.0.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets_1.0.0.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.3.0.v20130819-1623.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.3.0.v20130819-1623.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.9.1.v20130725-1141.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.9.1.v20130725-1141.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.8.101.v20130729-1318.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.8.101.v20130729-1318.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services_1.1.0.v20130515-1343.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services_1.1.0.v20130515-1343.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_3.5.0.v20120725-1805.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_3.5.0.v20120725-1805.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_4.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_4.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.6.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.6.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.300.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.300.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.2.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.2.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl2_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl2_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings_0.10.101.v20130801-2002.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings_0.10.101.v20130801-2002.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core_1.1.400.v20111202-1616.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core_1.1.400.v20111202-1616.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.1.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.1.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.1.200.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.1.200.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools_2.1.0.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools_2.1.0.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.8.100.v20130513-1637.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.8.100.v20130513-1637.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.105.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.105.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.4.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.4.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl2_3.2.600.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl2_3.2.600.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.102.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.102.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.3.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.3.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl2_4.3.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl2_4.3.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl1_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl1_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.300.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.300.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl2_0.10.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl2_0.10.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.105.0.v20130522-1122.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.105.0.v20130522-1122.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl2_2.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl2_2.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.inject_1.0.0.v20091030.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.inject_1.0.0.v20091030.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl2_1.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl2_1.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl2_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl2_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl2_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl2_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl1_0.11.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl1_0.11.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl1_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl1_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.105.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.105.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.4.200.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.4.200.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.500.v20130515-1343.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.500.v20130515-1343.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.105.1.v20130821-1411.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.105.1.v20130821-1411.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui_1.1.400.v20111007-1310.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui_1.1.400.v20111007-1310.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp_4.3.0.v20130911-1000.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp_4.3.0.v20130911-1000.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.core_3.5.0.v20120725-1805.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.core_3.5.0.v20120725-1805.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.8.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.8.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench_1.0.1.v20130910-2014.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench_1.0.1.v20130910-2014.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_4.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_4.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.2.0.v20130523-1557.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.2.0.v20130523-1557.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl2_0.11.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl2_0.11.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl2_1.3.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl2_1.3.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl1_2.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl1_2.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.200.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.200.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.8.101.v20130717-0806.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.8.101.v20130717-0806.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.xml_1.3.4.v201005080400.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.xml_1.3.4.v201005080400.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse_1.1.200.v20130516-1953.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse_1.1.200.v20130516-1953.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl2_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl2_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives_1.1.100.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives_1.1.100.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.200.v20130515-2028.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.200.v20130515-2028.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.util_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.util_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl1_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl1_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/introContent.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/introContent.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.api_description $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.api_description
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/preview.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/preview.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/migrate16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/migrate16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/whatsnew16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/whatsnew16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/samples16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/samples16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/webresources16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/webresources16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/tutorials16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/tutorials16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/firststeps16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/firststeps16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/overview16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/overview16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/sa_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/sa_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wr_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wr_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wn_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wn_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/fs_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/fs_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/mi_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/mi_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/tu_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/tu_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/banner_extension.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/banner_extension.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/ov_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/ov_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/workbench.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/workbench.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/arrow_rtl.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/arrow_rtl.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/background.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/background.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner_logo.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner_logo.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-relative.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-relative.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/migrate.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/migrate.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/webresources.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/webresources.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/firststeps.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/firststeps.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/tutorials.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/tutorials.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/standby.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/standby.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/rtl.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/rtl.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/ltr.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/ltr.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/samples.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/samples.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/whatsnew.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/whatsnew.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-absolute.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-absolute.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/overview.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/overview.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/shared.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/shared.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/root.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/root.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/firststeps.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/firststeps.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/whatsnew.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/whatsnew.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/standby.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/standby.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/samples.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/samples.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/root.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/root.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/migrate.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/migrate.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/tutorials.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/tutorials.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/overview.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/overview.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/webresources.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/webresources.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_med.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_med.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_med.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_med.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/grey_callout.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/grey_callout.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_high.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_high.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_high.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_high.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_high.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_high.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_high.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_high.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_med.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_med.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_med.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_med.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow_rtl.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow_rtl.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov_rtl.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov_rtl.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_rtl.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_rtl.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/html/shared.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/html/shared.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/preview.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/preview.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/migrate16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/migrate16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/overview.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/overview.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/webresources16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/webresources16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/samples.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/samples.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/firststeps16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/firststeps16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/tutorials.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/tutorials.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/whatsnew.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/whatsnew.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/background.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/background.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section2.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section2.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/whatsnew_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/whatsnew_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/firsteps_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/firsteps_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/migrate_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/migrate_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section4.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section4.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/backgroundcurve.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/backgroundcurve.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/webrsrc_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/webrsrc_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/samples_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/samples_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/overview_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/overview_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section1.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section1.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section3.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section3.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/tutorials_wtr.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/tutorials_wtr.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/background.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/background.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/brandmark.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/brandmark.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/dots.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/dots.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/newhov_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/newhov_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/new_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/new_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/back.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/back.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/forward.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/forward.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/home.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/home.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/back.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/back.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/wb48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/wb48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/forward.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/forward.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/home.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/home.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48sel.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48sel.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples72.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples72.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/back.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/back.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/forward.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/forward.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/swt/form_banner.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/swt/form_banner.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-relative.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-relative.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/migrate.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/migrate.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/webresources.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/webresources.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/firststeps.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/firststeps.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/tutorials.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/tutorials.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/standby.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/standby.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/rtl.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/rtl.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/ltr.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/ltr.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/samples.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/samples.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/whatsnew.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/whatsnew.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-absolute.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-absolute.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/overview.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/overview.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/shared.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/shared.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/root.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/root.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/firststeps.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/firststeps.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/whatsnew.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/whatsnew.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/standby.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/standby.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/samples.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/samples.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/root.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/root.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/migrate.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/migrate.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/tutorials.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/tutorials.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/overview.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/overview.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/webresources.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/webresources.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/preview.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/preview.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/migrate16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/migrate16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/whatsnew16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/whatsnew16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/samples16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/samples16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/webresources16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/webresources16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/tutorials16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/tutorials16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/firststeps16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/firststeps16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/overview16.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/overview16.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/sa_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/sa_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wr_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wr_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wn_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wn_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/fs_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/fs_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/mi_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/mi_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/tu_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/tu_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/ov_banner.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/ov_banner.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/newhov_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/newhov_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/new_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/new_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_onesample48.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_onesample48.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/arrow_rtl.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/arrow_rtl.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_bottomhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_bottomhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_bottomhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_bottomhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_bottomhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_bottomhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_32.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_32.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_midhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_midhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_rightedgehov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_rightedgehov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/content_nav_bar.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/content_nav_bar.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_rightedgehov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_rightedgehov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_bottomhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_bottomhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_64.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_64.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov2.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov2.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav.png $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav.png
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_hover.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_hover.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials_tophov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials_tophov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standbyhov.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standbyhov.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standby.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standby.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/rootpage/welcomebckgrd.jpg $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/rootpage/welcomebckgrd.jpg
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-relative.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-relative.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/migrate.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/migrate.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/webresources.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/webresources.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/firststeps.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/firststeps.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/tutorials.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/tutorials.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/standby.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/standby.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/rtl.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/rtl.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/ltr.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/ltr.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/samples.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/samples.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/whatsnew.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/whatsnew.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-absolute.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-absolute.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/overview.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/overview.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/shared.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/shared.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/root.css $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/root.css
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/firststeps.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/firststeps.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/whatsnew.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/whatsnew.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/standby.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/standby.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/samples.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/samples.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/root.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/root.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/migrate.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/migrate.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/tutorials.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/tutorials.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/overview.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/overview.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/webresources.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/webresources.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/welcome16.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/welcome16.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ihigh_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ihigh_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ilow_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ilow_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/inew_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/inew_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/icallout_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/icallout_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/image_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/image_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/extension_obj.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/extension_obj.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/elcl16/configure.gif $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/elcl16/configure.gif
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/universal.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/universal.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.options $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.options
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.200.v20130326-1255.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.200.v20130326-1255.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl1_2.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl1_2.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl1_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl1_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl1_1.0.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl1_1.0.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.4.1.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.4.1.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core_0.10.100.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core_0.10.100.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.8.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.8.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring_3.7.100.v20130605-1748.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring_3.7.100.v20130605-1748.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl1_0.12.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl1_0.12.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.6.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.6.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl2_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl2_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.400.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.400.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt_0.11.0.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt_0.11.0.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl2_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl2_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.2.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.2.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.300.v20130429-1813.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.300.v20130429-1813.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl2_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl2_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl2_3.5.401.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl2_3.5.401.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl1_4.3.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl1_4.3.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl2_3.4.500.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl2_3.4.500.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl2_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl2_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util_1.6.0.v201011041432.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util_1.6.0.v201011041432.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.svg_1.1.0.v201011041433.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.svg_1.1.0.v201011041433.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.102.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.102.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl1_0.10.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl1_0.10.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console_1.0.100.v20130429-0953.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console_1.0.100.v20130429-0953.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.200.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.200.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.400.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.400.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.9.0.v20130326-1255.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.9.0.v20130326-1255.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.500.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.500.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl1_3.6.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl1_3.6.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl1_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl1_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event_1.3.0.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event_1.3.0.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl2_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl2_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.300.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.300.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi_0.10.0.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi_0.10.0.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl1_3.5.401.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl1_3.5.401.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf_1.1.0.v20130516-1858.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf_1.1.0.v20130516-1858.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring_3.6.100.v20130605-1748.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring_3.6.100.v20130605-1748.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.jcraft.jsch_0.1.46.v201205102330.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.jcraft.jsch_0.1.46.v201205102330.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_4.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_4.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl1_3.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl1_3.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console_1.0.300.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console_1.0.300.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl1_0.10.2.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl1_0.10.2.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.css_1.6.0.v201011041432.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.css_1.6.0.v201011041432.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.200.v20130514-0954.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.200.v20130514-0954.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.5.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.5.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.sun.el_2.2.0.v201303151357.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.sun.el_2.2.0.v201303151357.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.100.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.100.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl1_1.0.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl1_1.0.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.2.100.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.2.100.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3_0.12.0.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3_0.12.0.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.100.v20130430-1352.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.100.v20130430-1352.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl1_3.7.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl1_3.7.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl1_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl1_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4_1.0.300.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4_1.0.300.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl2_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl2_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.5.100.v20130422-1538.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.5.100.v20130422-1538.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl_1.0.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl_1.0.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans_1.2.200.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans_1.2.200.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl2_0.11.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl2_0.11.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app_1.0.300.v20130819-1621.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app_1.0.300.v20130819-1621.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench_1.0.1.v20130909-1436.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench_1.0.1.v20130909-1436.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.2.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.2.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.8.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.8.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.exitroutines.api.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.exitroutines.api.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.native.jni.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.native.jni.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.cmdline.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.cmdline.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.bootstrap.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.bootstrap.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.common.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.common.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.102.1.v20130827-2048.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.102.1.v20130827-2048.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql_2.0.100.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql_2.0.100.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl2_0.10.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl2_0.10.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl2_2.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl2_2.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl2_3.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl2_3.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl1_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl1_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.3.0.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.3.0.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl2_0.10.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl2_0.10.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl1_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl1_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt_1.0.1.v20130823-1518.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt_1.0.1.v20130823-1518.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_3.0.0.v201112011016.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_3.0.0.v201112011016.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl1_1.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl1_1.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands_0.10.2.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands_0.10.2.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.4.0.v20130514-1240.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.4.0.v20130514-1240.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.8.0.v20130514-0954.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.8.0.v20130514-0954.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl1_1.1.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl1_1.1.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.3.0.v20130327-1440.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.3.0.v20130327-1440.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl2_0.12.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl2_0.12.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl2_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl2_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl1_1.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl1_1.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl2_3.6.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl2_3.6.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.2.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.2.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.3.0.v20130711-1809.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.3.0.v20130711-1809.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl2_3.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl2_3.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl2_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl2_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl2_1.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl2_1.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.6.0.v20130326-1254.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.6.0.v20130326-1254.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl2_1.0.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl2_1.0.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_4.0.0.v20130911-1000.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_4.0.0.v20130911-1000.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.300.v20130402-1529.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.300.v20130402-1529.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.2.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.2.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl1_0.12.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl1_0.12.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/DOM-LICENSE.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/DOM-LICENSE.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/LICENSE $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/LICENSE
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/NOTICE $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/NOTICE
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/SAX-LICENSE.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/SAX-LICENSE.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/ASL-LICENSE-2.0.txt $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/ASL-LICENSE-2.0.txt
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.pl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/envset.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/envset.cmd
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.cmd
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.py $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.py
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/lcp.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/lcp.bat
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.bat
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/complete-ant-cmd.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/complete-ant-cmd.pl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antenv.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antenv.cmd
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.bat
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.pl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runrc.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runrc.cmd
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/coverage-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/coverage-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-noframes.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-noframes.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/changelog.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/changelog.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/ant-bootstrap.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/ant-bootstrap.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/maudit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/maudit-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/tagdiff.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/tagdiff.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/log.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/log.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/mmetrics-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/mmetrics-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames-xalan1.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames-xalan1.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-text.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-text.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-frames.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-xdoc.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-xdoc.xsl
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/plugin.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-javamail.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-javamail.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-oro.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-oro.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bsf.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bsf.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jdepend.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jdepend.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-launcher.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-launcher.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jai.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jai.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-antlr.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-antlr.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit4.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit4.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-netrexx.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-netrexx.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-regexp.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-regexp.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-resolver.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-resolver.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-xalan2.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-xalan2.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-net.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-net.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jsch.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jsch.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bcel.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bcel.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-swing.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-swing.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-logging.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-logging.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jmf.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jmf.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-log4j.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-log4j.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-testutil.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-testutil.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.SF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/eclipse.inf
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.RSA
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.301.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.301.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt_0.11.1.v20130812-1345.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt_0.11.1.v20130812-1345.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.401.v20130709-1308.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.401.v20130709-1308.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.4.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.4.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.9.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.9.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.400.v20130326-1250.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.400.v20130326-1250.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl2_8.1.10.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl2_8.1.10.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl1_0.11.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl1_0.11.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_3.0.100.v20130327-1442.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_3.0.100.v20130327-1442.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.smil_1.0.0.v200806040011.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.smil_1.0.0.v200806040011.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.1.1.v201101211721.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.1.1.v201101211721.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.200.v20130327-2119.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.200.v20130327-2119.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.9.0.201308190730.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.9.0.201308190730.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.300.v20130514-1224.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.300.v20130514-1224.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore_2.9.1.v20130827-0309.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore_2.9.1.v20130827-0309.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.6.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.6.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.700.v20130402-1741.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.700.v20130402-1741.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl1_3.7.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl1_3.7.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl2_0.12.1.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl2_0.12.1.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.105.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.105.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl1_3.4.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl1_3.4.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core_3.7.0.v20130514-1224.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core_3.7.0.v20130514-1224.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_4.0.1.v20130708-1201.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_4.0.1.v20130708-1201.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.3.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.3.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.5.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.5.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_5.0.0.v20130604-1622.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_5.0.0.v20130604-1622.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.3.5.v201308161310.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.3.5.v201308161310.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl2_3.9.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl2_3.9.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.200.v20130402-1505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.200.v20130402-1505.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.6.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.6.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.300.v20130225-1821.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.300.v20130225-1821.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_4.0.0.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_4.0.0.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl1_1.0.400.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl1_1.0.400.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.300.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.300.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.4.101.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.4.101.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl2_1.1.200.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl2_1.1.200.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_3.0.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_3.0.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.2.0.v201112011158.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.2.0.v201112011158.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_8.0.0.201510221435.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_8.0.0.201510221435.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.100.v20130515-1857.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.100.v20130515-1857.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.100.v20130430-1334.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.100.v20130430-1334.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet_8.1.10.v20130312.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet_8.1.10.v20130312.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.200.v20130326-1254.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.200.v20130326-1254.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.100.v201309250800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.100.v201309250800.jar
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/icon.xpm $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/icon.xpm
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about.html
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/artifacts.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/artifacts.xml
install /build/slot1/p800_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/runwithtrace $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/runwithtrace

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(555,mqm,mqm) "/opt/mqm/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/html"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/swt"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/rootpage"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/elcl16"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/links"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/dropins"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer-8.0.0.cmptag"
%attr(555,mqm,mqm) "/opt/mqm/bin/strmqcfg"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/MQExplorer"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/bin/MQExplorer.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/configuration/config.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_8.0.0.201510221435"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product.root.feature_root_8.0.0.201510221436"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_8.0.0.201510221436"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131138.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521131502.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521148184.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1445521146694.profile.gz"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs"
%attr(444,mqm,mqm) %verify(not md5 mtime size) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/.eclipseproduct"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls2_2.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls2_2.0.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls1_1.2.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls1_1.2.0.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help_2.0.1.v20130911-1000/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.help.nls1_2.0.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature.nls2_1.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls1_1.2.0.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls2_2.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls1_3.9.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp_4.3.1.v20130911-1000/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_fr.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ru.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_id.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_gr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_tr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_CN.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_es.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_si.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ja_JP.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_TW.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_it.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pt_BR.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_hu.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_zh_CN.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_pl.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ko.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/category.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_de.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_ja_JP.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/license_cs.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_8.0.0.201510221435/feature_lt.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature.nls2_1.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common.nls1_2.9.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui_2.2.0.v20130828-0031/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore_2.9.1.v20130827-0309/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform_4.3.1.v20130911-1000/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls2_4.3.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.ecore.nls1_2.9.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature_1.2.1.v20130827-1605/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls2_1.2.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls2_2.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.extras.feature_1.2.0.v20130827-1605/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.core.feature.nls2_1.2.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp.nls1_1.2.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_id.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_gr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_tr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_CN.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_si.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ja_JP.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_8.0.0.201510221435/feature_lt.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.user.ui.nls1_2.2.0.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls2_4.3.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.rcp.nls1_4.3.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.emf.common_2.9.1.v20130827-0309/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.e4.rcp_1.2.1.v20130910-2014/eclipse_update_120.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d.nls2_3.9.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_8.0.0.201510221435/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.platform.nls1_4.3.1.v201309250800/feature_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/maven/org.eclipse.draw2d.features/org.eclipse.draw2d/pom.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.draw2d_3.9.1.201308190730/eclipse_update_120.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/org.eclipse.equinox.p2.rcp.feature_1.2.0.v20130828-0031/epl-v10.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl1_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl2_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.4.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl2_3.7.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.2.0.v20130828-0031.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.4.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl1_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui_3.7.1.v20130729-1104.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.command_0.10.0.v201209301215.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user_4.3.0.v20130605-1059.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.301.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.100.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/launcher.gtk.linux.x86_64.properties"
%attr(444,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/eclipse_1506.so"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.200.v20130807-1835/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.102.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclientprereqs_8.0.0.4.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util.gui_1.6.0.v201011041432.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_5.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.shell_0.10.0.v201212101605.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.500.v20130404-1337.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.6.100.v20130326-1250.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util_3.2.300.v20130513-1956.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_50.1.1.v201304230130.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.200.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.io_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl2_0.11.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl1_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.102.1.v20130827-2021.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.200.v20130419-1850.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.4.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.9.1.v20130704-1828.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di_1.0.0.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.2.0.v20130424-1801.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.css.sac_1.3.1.v200903091627.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets_3.4.200.v20130326-1254.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.3.0.v20130509-0110.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common.nl2_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi_2.9.1.v20130827-0309.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.8.101.v20130802-1147.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.300.v20130517-0139.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl1_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl1_3.4.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl1_4.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.3.100.v20130513-1956.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.4.1.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl2_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt_0.12.1.v20130815-1438.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl1_0.9.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.3.5.v201308161310.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl2_4.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl1_3.2.600.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl2_3.4.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.doc.user.nl2_4.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser_3.4.100.v20130527-1656.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl1_2.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.1.100.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl1_3.2.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme.nl2_0.9.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_5.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl2_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql.nl2_2.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.200.v20130326-1255.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.4.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl2_3.7.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di_1.3.0.v20130514-1256.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.common_2.9.1.v20130827-0309.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl1_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.4.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpcore_4.1.4.v201203221030.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.300.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.felix.gogo.runtime_0.10.0.v201209301036.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.ui.nl1_3.7.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.400.v20130418-1354.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl1_0.10.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.105.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_3.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl2_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl1_3.6.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl1_0.11.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.9.0.v20130516-1713.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl1_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.4.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl1_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools_1.0.200.v20130402-1741.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.security_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl2_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl1_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.6.200.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.3.0.v20130412-2032.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services_1.0.1.v20130909-1436.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts_1.3.1.v20130905-0905.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.300.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl1_0.10.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport_1.1.0.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.700.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources_3.4.500.v20130516-1049.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.6.1.v20130822-1117.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl1_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.100.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.theme_0.9.100.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl1_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.services.nl2_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.di.nl2_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.5.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui.nl1_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools_3.2.200.v20130508-2007.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl1_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.httpcomponents.httpclient_4.1.3.v201209201135.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.3.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench.nl2_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.continuation_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl2_3.7.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions_0.11.100.v20130514-1256.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.3.0.v20130526-2122.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite_1.0.400.v20130515-2028.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.6.200.v20130514-1258.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.3.0.v20130526-0335.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.9.1.v20130814-1242.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl2_2.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.6.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.200.v20130430-1352.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change_2.9.0.v20130827-0309.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.102.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.301.v20130717-1549.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl1_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.annotation_1.1.0.v201209060031.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.500.v20130402-1746.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.4.101.v20130813-1853.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.osgi.allclient_8.0.0.4.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.codec_1.4.0.v201209201156.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.8.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed_3.6.0.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.util.nl2_3.2.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl1_1.3.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net_1.2.200.v20120807-0927.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper.glassfish_2.2.2.v201205150955.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.200.v20130326-1319.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.700.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http.nl2_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/splash.bmp"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/helpData.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro-eclipse.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse_lg.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/.api_description"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/narrow_book.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/disabled_book.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin_customization.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/tutorialsExtensionContent.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/overviewExtensionContent.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent1.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent2.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/tutorials.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/whatsnew.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/css/overview.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/intro/whatsnewExtensionContent3.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_merge.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/cheatsheets/cvs_checkout.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/platform.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/introData.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_win7.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_blu.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_basestyle.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_gtk.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_win7.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_classic_winxp.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mru_on_win7.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_winxp_olv.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/css/e4_default_mac.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkTSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/arrow.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_updates48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_migrate48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_wbbasics48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclplatform48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_merge48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/tu_checkout48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/ov_teamsup48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/topiclabel/wn_eclcommunity48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macGrey.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/dragHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkGrey.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7Handle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPTSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macTSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPOlive.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBluTSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/gtkHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicTSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPBlue.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winXPHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/winClassicHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/macHandle.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/images/win7TSFrame.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse32.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/macosx_narrow_book.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/book.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/plugin.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.mappings"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/LegacyIDE.e4xmi"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/eclipse256.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform_4.3.1.v20130911-1000/about.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.nl1_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.300.v20130503-1750.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp.nl1_4.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl1_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.5.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/.api_description"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.5.200.v20130514-1256/runtime_registry_compatibility.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.nl2_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl2_0.10.2.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search_3.9.0.v20130312-1625.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.300.v20130515-1451.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.8.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl1_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.el_2.2.0.v201303151357.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets_1.0.0.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.3.0.v20130819-1623.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.9.1.v20130725-1141.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.8.101.v20130729-1318.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services_1.1.0.v20130515-1343.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_3.5.0.v20120725-1805.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_4.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.6.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.300.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.2.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.xmi.nl2_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings_0.10.101.v20130801-2002.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core_1.1.400.v20111202-1616.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.1.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.1.200.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools_2.1.0.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.8.100.v20130513-1637.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.105.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.4.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal.nl2_3.2.600.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.102.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.3.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl2_4.3.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl1_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.300.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl2_0.10.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.105.0.v20130522-1122.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl2_2.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.inject_1.0.0.v20091030.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl2_1.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui.nl2_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl2_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.di.extensions.nl1_0.11.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl1_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.105.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.4.200.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.500.v20130515-1343.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.105.1.v20130821-1411.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.ui_1.1.400.v20111007-1310.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.rcp_4.3.0.v20130911-1000.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.core_3.5.0.v20120725-1805.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.8.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench_1.0.1.v20130910-2014.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_4.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.2.0.v20130523-1557.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt.nl2_0.11.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.contexts.nl2_1.3.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.tools.nl1_2.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.200.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.8.101.v20130717-0806.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.xml_1.3.4.v201005080400.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse_1.1.200.v20130516-1953.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl2_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives_1.1.100.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.200.v20130515-2028.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.util_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl1_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/introContent.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.api_description"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/preview.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/migrate16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/whatsnew16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/samples16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/webresources16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/tutorials16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/firststeps16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/launchbar/overview16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/sa_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wr_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/wn_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/fs_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/mi_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/tu_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/banner_extension.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/contentpage/ov_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/workbench.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/arrow_rtl.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wn_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tu_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wb_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/migrate-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/overview.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/wr_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/whatsnew.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/ov_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/sa_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/webresources-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/tutorials.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/mi_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/fs_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/firststeps-select.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/icons/ctool/samples-select.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/mi_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/sa_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/tu_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wr_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wn_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/wb_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/fs_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/standby/ov_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/background.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/samples48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/tutorials48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/overview48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/root_banner_logo.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48_hov.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/webresources48_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/workbench48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/whatsnew48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/firststeps48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/graphics/rootpage/migrate48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-relative.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/migrate.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/webresources.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/firststeps.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/tutorials.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/standby.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/rtl.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/ltr.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/samples.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/whatsnew.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/font-absolute.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/overview.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/shared.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/html/root.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/firststeps.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/whatsnew.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/standby.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/samples.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/root.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/migrate.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/tutorials.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/overview.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/slate/swt/webresources.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_med.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_med.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/grey_callout.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_high.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/ov_high.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_high.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wr-mi_high.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/tu-sa_med.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/contentpage/wn-fs_med.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/arrow_rtl.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_open.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov_rtl.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_rtl.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/graphics/icons/ctool/widget_closed_hov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/shared/html/shared.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/preview.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/migrate16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/overview.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/webresources16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/samples.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/firststeps16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/tutorials.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/launchbar/whatsnew.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/background.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section2.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/whatsnew_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/firsteps_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/migrate_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section4.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/backgroundcurve.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/webrsrc_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/samples_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/overview_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section1.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/section3.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/contentpage/tutorials_wtr.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/background.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/root/dots.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/newhov_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/obj48/new_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/back.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/wb48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/whatsnew48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/forward.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/home.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/firsteps16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/webrsrc72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/tutorials48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/overview16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/samples72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/ctool/migrate48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/back.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/wb48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/whatsnew48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/overview72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/forward.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/home.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/firsteps48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/webrsrc72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/tutorials48sel.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/samples72.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/etool/migrate48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/back.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/icons/dtool/forward.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/graphics/swt/form_banner.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-relative.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/migrate.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/webresources.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/firststeps.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/tutorials.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/standby.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/rtl.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/ltr.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/samples.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/whatsnew.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/font-absolute.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/overview.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/shared.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/html/root.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/firststeps.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/whatsnew.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/standby.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/samples.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/root.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/migrate.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/tutorials.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/overview.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/purpleMesh/swt/webresources.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/preview.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/migrate16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/whatsnew16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/samples16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/webresources16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/tutorials16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/firststeps16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/launchbar/overview16.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/sa_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wr_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/wn_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/fs_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/mi_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/tu_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/contentpage/ov_banner.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/newhov_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/obj48/new_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_onesample48.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/webresources.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/arrow_rtl.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_bottomhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/samples.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/migrate.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wn_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tu_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wb_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/workbench_bottomhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/overview_bottomhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/wr_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_32.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_midhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/nav_rightedgehov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/whatsnew.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/content_nav_bar.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/sa_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_rightedgehov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/cpt_bottomhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/ov_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_64.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/root_midhov2.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/mi_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/firststeps.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav.png"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/fs_nav_hover.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/icons/ctool/tutorials_tophov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/mi_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/sa_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/tu_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wr_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wn_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/wb_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/fs_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standbyhov.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/standby/ov_standby.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/graphics/rootpage/welcomebckgrd.jpg"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-relative.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/migrate.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/webresources.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/firststeps.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/tutorials.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/standby.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/rtl.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/ltr.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/samples.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/whatsnew.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/font-absolute.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/overview.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/shared.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/html/root.css"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/firststeps.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/whatsnew.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/standby.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/samples.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/root.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/migrate.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/tutorials.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/overview.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/themes/circles/swt/webresources.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/welcome16.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ihigh_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/ilow_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/inew_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/icallout_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/image_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/obj16/extension_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/icons/full/elcl16/configure.gif"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/universal.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/plugin.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.universal_3.2.600.v20130326-1254/.options"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.200.v20130326-1255.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.change.nl1_2.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jsch.core.nl1_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl1_1.0.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.4.1.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core_0.10.100.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.8.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring_3.7.100.v20130605-1748.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl1_0.12.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.6.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl2_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.400.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt_0.11.0.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl2_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.2.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.300.v20130429-1813.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event.nl2_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl2_3.5.401.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.platform.nl1_4.3.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.resources.nl2_3.4.500.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.services.nl2_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.util_1.6.0.v201011041432.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.svg_1.1.0.v201011041433.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.102.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.core.nl1_0.10.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console_1.0.100.v20130429-0953.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.200.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.400.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.9.0.v20130326-1255.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.500.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring.nl1_3.6.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl.nl1_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.event_1.3.0.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.server.nl2_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.300.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi_0.10.0.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.nl1_3.5.401.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf_1.1.0.v20130516-1858.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.core.refactoring_3.6.100.v20130605-1748.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.jcraft.jsch_0.1.46.v201205102330.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_4.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl1_3.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.console_1.0.300.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands.nl1_0.10.2.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.batik.css_1.6.0.v201011041432.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.200.v20130514-0954.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.5.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.sun.el_2.2.0.v201303151357.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.100.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt.nl1_1.0.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.2.100.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3_0.12.0.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.100.v20130430-1352.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ltk.ui.refactoring.nl1_3.7.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl1_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4_1.0.300.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.transport.ecf.nl2_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.5.100.v20130422-1538.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.security.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.httpclient4.ssl_1.0.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans_1.2.200.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl2_0.11.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.app_1.0.300.v20130819-1621.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.model.workbench_1.0.1.v20130909-1436.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.2.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.8.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.exitroutines.api.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.native.jni.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.cmdline.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.bootstrap.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/lib/com.ibm.wmqfte.common.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_8.0.0.201510221435/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.102.1.v20130827-2048.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ql_2.0.100.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.bidi.nl2_0.10.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore.nl2_2.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.externaltools.nl2_3.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl1_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.3.0.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.bindings.nl2_0.10.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl1_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.addons.swt_1.0.1.v20130823-1518.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_3.0.0.v201112011016.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.widgets.nl1_1.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.core.commands_0.10.2.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.4.0.v20130514-1240.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.8.0.v20130514-0954.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.importexport.nl1_1.1.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.3.0.v20130327-1440.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl2_0.12.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.natives.nl2_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.console.nl1_1.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.properties.tabbed.nl2_3.6.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.2.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.3.0.v20130711-1809.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.cheatsheets.nl2_3.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.beans.nl2_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.net.nl2_1.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.6.0.v20130326-1254.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.externaltools.nl2_1.0.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_4.0.0.v20130911-1000.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.300.v20130402-1529.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.2.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench3.nl1_0.12.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/DOM-LICENSE.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/LICENSE"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/NOTICE"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/SAX-LICENSE.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about_files/ASL-LICENSE-2.0.txt"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/envset.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runant.py"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/lcp.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/complete-ant-cmd.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antenv.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/ant.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/antRun.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/bin/runrc.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/coverage-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-noframes.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/changelog.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/ant-bootstrap.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/maudit-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/jdepend-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/tagdiff.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/log.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/mmetrics-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/junit-frames-xalan1.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-text.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/etc/checkstyle/checkstyle-xdoc.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-javamail.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-oro.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bsf.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jdepend.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-launcher.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jai.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-antlr.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-junit4.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-netrexx.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-regexp.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-resolver.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-xalan2.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-net.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jsch.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-bcel.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-swing.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-commons-logging.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-jmf.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-apache-log4j.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/lib/ant-testutil.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.8.4.v201303080030/META-INF/ECLIPSE_.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.301.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.renderers.swt_0.11.1.v20130812-1345.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.401.v20130709-1308.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.4.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.9.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.400.v20130326-1250.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet.nl2_8.1.10.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.css.swt.nl1_0.11.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_3.0.100.v20130327-1442.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.w3c.dom.smil_1.0.0.v200806040011.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.1.1.v201101211721.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.200.v20130327-2119.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.9.0.201308190730.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.300.v20130514-1224.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.emf.ecore_2.9.1.v20130827-0309.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.6.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.700.v20130402-1741.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.http_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core.nl1_3.7.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.e4.ui.workbench.swt.nl2_0.12.1.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.105.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.browser.nl1_3.4.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.team.core_3.7.0.v20130514-1224.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_4.0.1.v20130708-1201.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.3.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.5.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_5.0.0.v20130604-1622.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.3.5.v201308161310.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.search.nl2_3.9.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.200.v20130402-1505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.6.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.300.v20130225-1821.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_4.0.0.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatesite.nl1_1.0.400.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.300.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.4.101.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.eclipse.nl2_1.1.200.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_3.0.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.2.0.v201112011158.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_8.0.0.201510221435.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.100.v20130515-1857.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.100.v20130430-1334.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jetty.servlet_8.1.10.v20130312.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.200.v20130326-1254.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.100.v201309250800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/icon.xpm"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/artifacts.xml"
%attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/runwithtrace"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="8.0.0"
RPM_PACKAGE_RELEASE="4"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
if [ -z "${RPM_INSTALL_PREFIX}" ] ; then 
  MQ_INSTALLATION_PATH=${MQ_DEFAULT_INSTALLATION_PATH}
else
  MQ_INSTALLATION_PATH=`echo "${RPM_INSTALL_PREFIX}" | sed s#//#/#g`
fi
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p800-004-151022.DE su=_8QwZKXivEeWg74sVC8pxOw pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72" 
#   years="2005,2015" 
#   crc="3530070126" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72 
#    
#   (C) Copyright IBM Corp. 2005, 2015 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 
# 
# Common Preinstallation script for all packages
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 
#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified installation path (${MQ_INSTALLATION_PATH}) exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo "${MQ_INSTALLATION_PATH}" | grep "[:%# ]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified installation path (${MQ_INSTALLATION_PATH}) contains an unsupported character"
  echo ""
  exit 1
fi
# Trailing blanks 
echo "${MQ_INSTALLATION_PATH}" | grep "\ $" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified installation path (${MQ_INSTALLATION_PATH}) contains an unsupported character"
  echo ""
  exit 1
fi


#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty 
  # ignore lost+found and .snapshots(GPFS) directories
  # The .snapshots directory can also be renamed within GPFS, so we allow an alternate name to be specified with
  # AMQ_IGNORE_SNAPDIRNAME
  #####################################################################################################
  if [ x${AMQ_OVERRIDE_EMPTY_INSTALL_PATH} = x ] ;then 
    if [ -d ${MQ_INSTALLATION_PATH} ] && [ ${MQ_INSTALLATION_PATH} != ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then
      if [ "${AMQ_IGNORE_SNAPDIRNAME}" = "" ] ; then
        SNAPDIR_NAME=".snapshots"
      else
        SNAPDIR_NAME="${AMQ_IGNORE_SNAPDIRNAME}"
      fi
      LS_ALL=`ls -A ${MQ_INSTALLATION_PATH} 2>/dev/null | grep -F -v "lost+found" | grep -F -v "${SNAPDIR_NAME}"`
      if [ "${LS_ALL}" ] ; then
        echo ""
        echo "ERROR:   Specified installation path '${MQ_INSTALLATION_PATH}' is not empty"
        echo ""
        exit 1
     fi
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#######################################################################################################
# Preventing an installation over an existing installation
# Each component has a unique '.cmptag' file.  If this is already present on the filesystem at the
# installation location, then the component must already be installed to this location, so we should
# abort.
#######################################################################################################
case "${RPM_PACKAGE_NAME}" in
  MQSeriesAMS)
      compfile="IBM_WebSphere_MQ_Advanced_Message_Security_Component."
      ;;
  MQSeriesASOAP)
      compfile="DOES_NOT_CONTAIN_A_COMPONENT_FILE"
      ;;
  MQSeriesAMQP)
      compfile="DOES_NOT_CONTAIN_A_COMPONENT_FILE"
      ;;
  MQSeriesClient)
      compfile="IBM_WebSphere_MQ_Client."
      ;;
  MQSeriesExplorer)
      compfile="IBM_WebSphere_MQ_Explorer."
      ;;
  MQSeriesFTAgent)
      compfile="IBM_WebSphere_MQ_Managed_File_Transfer_Agent."
      ;;
  MQSeriesFTBase)
      compfile="IBM_WebSphere_MQ_Managed_File_Transfer_Base."
      ;;
  MQSeriesFTLogger)
      compfile="IBM_WebSphere_MQ_Managed_File_Transfer_Logger."
      ;;
  MQSeriesFTService)
      compfile="IBM_WebSphere_MQ_Managed_File_Transfer_Service."
      ;;
  MQSeriesFTTools)
      compfile="IBM_WebSphere_MQ_Managed_File_Transfer_Tools."
      ;;
  MQSeriesGSKit)
      compfile='IBM_WebSphere_MQ_GSKit.'
      ;;
  MQSeriesJava)
      compfile="IBM_WebSphere_MQ_Java_Messaging."
      ;;
  MQSeriesJRE)
      compfile="IBM_WebSphere_MQ_JRE"
      ;;
  MQSeriesMan)
      compfile="IBM_WebSphere_MQ_Man_Pages."
      ;;
  MQSeriesMsg_cs)
      compfile="IBM_WebSphere_MQ_Messages_Czech."
      ;;
  MQSeriesMsg_de)
      compfile="IBM_WebSphere_MQ_Messages_German."
      ;;
  MQSeriesMsg_es)
      compfile="IBM_WebSphere_MQ_Messages_Spanish."
      ;;
  MQSeriesMsg_fr)
      compfile="IBM_WebSphere_MQ_Messages_French."
      ;;
  MQSeriesMsg_hu)
      compfile="IBM_WebSphere_MQ_Messages_Hungarian."
      ;;
  MQSeriesMsg_it)
      compfile="IBM_WebSphere_MQ_Messages_Italian."
      ;;
  MQSeriesMsg_ja)
      compfile="IBM_WebSphere_MQ_Messages_Japanese."
      ;;
  MQSeriesMsg_ko)
      compfile="IBM_WebSphere_MQ_Messages_Korean."
      ;;
  MQSeriesMsg_pl)
      compfile="IBM_WebSphere_MQ_Messages_Polish."
      ;;
  MQSeriesMsg_pt)
      compfile="IBM_WebSphere_MQ_Messages_Brazilian_Portuguese."
      ;;
  MQSeriesMsg_ru)
      compfile="IBM_WebSphere_MQ_Messages_Russian."
      ;;
  MQSeriesMsg_Zh_CN)
      compfile="IBM_WebSphere_MQ_Messages_Chinese_Simplified."
      ;;
  MQSeriesMsg_Zh_TW)
      compfile="IBM_WebSphere_MQ_Messages_Chinese_Traditional."
      ;;
  MQSeriesRuntime)
      compfile="IBM_WebSphere_MQ_Runtime."
      ;;
  MQSeriesSamples)
      compfile="IBM_WebSphere_MQ_Samples."
      ;;
  MQSeriesSDK)
      compfile="IBM_WebSphere_MQ_SDK."
      ;;
  MQSeriesServer)
      compfile="IBM_WebSphere_MQ_Server."
      ;;
  MQSeriesXRService)
      compfile="IBM_WebSphere_MQ_Telemetry_Service."
      ;;
  *)
      echo "ERROR: Package name ${RPM_PACKAGE_NAME} not recognised, aborting installation."
      exit 1
      ;;
esac

# RPM_PACKAGE_VERSION is of the form 8.0.0
# If the 'compfile' file is present, then the package is already installed on
# the system, and we abort the installation of this package.
# NOTE the careful positioning of the wildcard outside the doublequotes.
if [ -f "${MQ_INSTALLATION_PATH}/properties/version/${compfile}"* ]; then
  echo "ERROR:  The specified installation path (${MQ_INSTALLATION_PATH}) already"
  echo "        has this package (${RPM_PACKAGE_NAME}) installed."
  echo "        Aborting installation."
  exit 1
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72, 
#    
#   (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r "${MQ_INSTALLATION_PATH}/licenses/status.dat" ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="8.0.0"
RPM_PACKAGE_RELEASE="4"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
if [ -z "${RPM_INSTALL_PREFIX}" ] ; then 
  MQ_INSTALLATION_PATH=${MQ_DEFAULT_INSTALLATION_PATH}
else
  MQ_INSTALLATION_PATH=`echo "${RPM_INSTALL_PREFIX}" | sed s#//#/#g`
fi
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi

# @(#) MQMBID sn=p800-004-151022.DE su=_8QwZKXivEeWg74sVC8pxOw pn=install/unix/linux_2/mqexplorer_postinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2011,2012" 
#   crc="1016225224" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72, 
#    
#   (C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 

EXPLORER_DIR=${MQ_INSTALLATION_PATH}/mqexplorer/eclipse

# Update ini file to point to 'bin' directory of installed JRE
INI_FILE=${MQ_INSTALLATION_PATH}/bin/MQExplorer.ini
if [ -f "${INI_FILE}" ]
then
  # need to use 64bit JRE on 64bit platforms
  JRE_DIR=${MQ_INSTALLATION_PATH}/java/jre64/jre/bin
  if [ ! -d "${JRE_DIR}" ]
  then
    JRE_DIR=${MQ_INSTALLATION_PATH}/java/jre/jre/bin
  fi
  # replace -vm path within ini file
  chmod 644 "${INI_FILE}"
  sed -i -e "s#../java/jre/bin#${JRE_DIR}#" "${INI_FILE}"
  chmod 444 "${INI_FILE}"
fi

# Query the name of this installation
INSTALLATION_NAME=default
OUTPUT=`"${MQ_INSTALLATION_PATH}/bin/dspmqver" -b -f 512`
if [ $? -eq 0 ]
then
  # ensure name contains only letters and digits
  INVALID_CHARS=`echo -n "${OUTPUT}" | tr -d "[:alnum:]"`
  if [ -z "${INVALID_CHARS}" ]
  then
    INSTALLATION_NAME=${OUTPUT}
  fi
fi

# Update config to qualify workspace directory with installation name
CONFIG_FILE=${EXPLORER_DIR}/configuration/config.ini
if [ -f "${CONFIG_FILE}" ]
then
  # Add "-" and installation name to config file to change the workspace location
  chmod 644 "${CONFIG_FILE}"
  sed -i -e "s#IBM/WebSphereMQ/workspace#IBM/WebSphereMQ/workspace-${INSTALLATION_NAME}#" "${CONFIG_FILE}"
  # Also set eclipse.home.location appropriately
  sed -i -e "s#@MQ.INSTALLATION.PATH#${MQ_INSTALLATION_PATH}#" "${CONFIG_FILE}"
  chmod 444 "${CONFIG_FILE}"
fi

# Contribute to system menus
DESKTOP_DIR=/usr/share/applications
if [ -d "${DESKTOP_DIR}" ]
then
  # Write out .desktop file qualified by installation name
  DESKTOP_FILE=${DESKTOP_DIR}/IBM_WebSphere_MQ_Explorer-${INSTALLATION_NAME}.desktop
  echo "[Desktop Entry]" > "${DESKTOP_FILE}"
  echo "Version=1.0" >> "${DESKTOP_FILE}"
  echo "Type=Application" >> "${DESKTOP_FILE}"
  echo "Encoding=UTF-8" >> "${DESKTOP_FILE}"
  echo "Exec=${MQ_INSTALLATION_PATH}/bin/MQExplorer" >> "${DESKTOP_FILE}"
  echo "Icon=${EXPLORER_DIR}/icon.xpm" >> "${DESKTOP_FILE}"
  echo "Name=IBM WebSphere MQ Explorer (${INSTALLATION_NAME})" >> "${DESKTOP_FILE}"
  echo "Comment=IBM WebSphere MQ Explorer (${INSTALLATION_NAME})" >> "${DESKTOP_FILE}"
  echo "Categories=Development" >> "${DESKTOP_FILE}"
  chmod 644 "${DESKTOP_FILE}"
fi

# Create configuration in install directory using MQExplorer -initialize

if [ -x ${MQ_INSTALLATION_PATH}/bin/MQExplorer ] ; then 
# Ensure no windows are opened
  unset DISPLAY 
  ${MQ_INSTALLATION_PATH}/bin/MQExplorer -initialize -nosplash -data /tmp/$$workspace
  rm -rf /tmp/$$workspace
  if [ -d ${EXPLORER_DIR}/configuration ] ; then 
    chown -R mqm:mqm ${EXPLORER_DIR}/configuration
    chmod -R 444 ${EXPLORER_DIR}/configuration
    find ${EXPLORER_DIR}/configuration -type d -print0 | xargs -0 chmod 555
  fi
  if [ -d ${EXPLORER_DIR}/p2 ] ; then 
    chown -R mqm:mqm ${EXPLORER_DIR}/p2
    chmod -R 444 ${EXPLORER_DIR}/p2
    find ${EXPLORER_DIR}/p2 -type d -print0 | xargs -0 chmod 555
  fi
fi

# Invoke setmqinst to refresh symlinks if this installation is the primary
setmqinst_out=`LD_LIBRARY_PATH="" ${MQ_INSTALLATION_PATH}/bin/setmqinst -r -p ${MQ_INSTALLATION_PATH} 2>&1`
rc=$?
if [ $rc -ne 0 ] ; then
  echo "ERROR: Return code \"$rc\" from setmqinst for \"-r -p ${MQ_INSTALLATION_PATH}\", output is:" >&2
  echo "       ${setmqinst_out}" >&2
fi
echo > /dev/null


%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="8.0.0"
RPM_PACKAGE_RELEASE="4"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
if [ -z "${RPM_INSTALL_PREFIX}" ] ; then 
  MQ_INSTALLATION_PATH=${MQ_DEFAULT_INSTALLATION_PATH}
else
  MQ_INSTALLATION_PATH=`echo "${RPM_INSTALL_PREFIX}" | sed s#//#/#g`
fi
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72" 
#   years="2005,2014" 
#   crc="122768040" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72 
#    
#   (C) Copyright IBM Corp. 2005, 2014 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -v -x > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ] 
    then
        echo " " >&2
        echo "ERROR: WebSphere MQ shared resources associated with the installation at" >&2
        echo "      '${MQ_INSTALLATION_PATH}' are still in use." >&2
        echo "       You must stop all MQ processing, including applications, Queue Managers" >&2 
        echo "       and Listeners before attempting to install, update or delete" >&2
        echo "       the WebSphere MQ product." >&2
        echo " " >&2
        echo "       'amqiclen -v -x' return code was: '$amqiclen_rc', output was:" >&2
        cat /tmp/amqiclen.$$.out >&2
        echo " " >&2
        rm -f /tmp/amqiclen.$$.out
        exit 1
    fi
    rm -f /tmp/amqiclen.$$.out
fi 
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72, 
#    
#   (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

unset fix_exists

fix_exists=$(find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      echo  $num_files
      exit
    fi
  fi
done
)
if [ ! -z $fix_exists ] ; then 
  echo "ERROR:  There appears to be a fixpack installed on this machine for this" >&2
  echo "        component." >&2
  echo "" >&2
  echo "        Please ensure you have removed all fixpacks for the ${RPM_PACKAGE_NAME}" >&2
  echo "        component before trying to remove this package." >&2
  echo "" >&2
  exit 1 
fi

# Removing product links

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="8.0.0"
RPM_PACKAGE_RELEASE="4"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
if [ -z "${RPM_INSTALL_PREFIX}" ] ; then 
  MQ_INSTALLATION_PATH=${MQ_DEFAULT_INSTALLATION_PATH}
else
  MQ_INSTALLATION_PATH=`echo "${RPM_INSTALL_PREFIX}" | sed s#//#/#g`
fi
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p800-004-151022.DE su=_8QwZKXivEeWg74sVC8pxOw pn=install/unix/linux_2/mqexplorer_postuninstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2011,2015" 
#   crc="931196921" > 
#   Licensed Materials - Property of IBM  
#    
#   5724-H72, 
#    
#   (C) Copyright IBM Corp. 2011, 2015 All Rights Reserved.  
#    
#   US Government Users Restricted Rights - Use, duplication or  
#   disclosure restricted by GSA ADP Schedule Contract with  
#   IBM Corp.  
#   </copyright> 

EXPLORER_DIR=${MQ_INSTALLATION_PATH}/mqexplorer/eclipse

# Remove any extra configuration data which may have been created
if [ -d ${EXPLORER_DIR}/configuration ] ; then 
  rm -Rf ${EXPLORER_DIR}/configuration
fi
if [ -d ${EXPLORER_DIR}/p2 ] ; then 
  rm -Rf ${EXPLORER_DIR}/p2
fi

rmdir ${MQ_INSTALLATION_PATH}/mqexplorer/eclipse  > /dev/null 2>&1
rmdir ${MQ_INSTALLATION_PATH}/mqexplorer > /dev/null 2>&1

# Remove any system menus entries referring to this installation
DESKTOP_DIR=/usr/share/applications
if [ -d "${DESKTOP_DIR}" ]
then
  for file in `ls ${DESKTOP_DIR}/IBM_WebSphere_MQ_Explorer-*.desktop`
  do
    grep "Exec=${MQ_INSTALLATION_PATH}/bin/MQExplorer" "${file}" >/dev/null
    if [ $? -eq 0 ]
    then
      # a line matched so remove the file
      rm -f "${file}"
    fi
  done
fi

