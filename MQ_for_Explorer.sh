#!/bin/bash
mkdir -p /tmp/mq_license_8.0.0/license/ 
echo -e "#rava\nstatus=9\n"  > /tmp/mq_license_8.0.0/license/status.dat 
mkdir ./MQExplorer
cp ./MQServer/MQSeriesRuntime*.rpm ./MQExplorer
cp ./MQServer/MQSeriesJRE*.rpm ./MQExplorer 
cp ./MQServer/MQSeriesGSKit*.rpm ./MQExplorer
cp ./MQServer/MQSeriesExplorer*.rpm ./MQExplorer
rpm -ivh ./MQExplorer/*.rpm
