#!/bin/bash
tar -xzvf mqadv_dev80_linux_x86-64.tar.gz
mkdir -p ./MQ
cp ./MQServer/MQSeriesRuntime*.rpm ./MQ
cp ./MQServer/MQSeriesServer*.rpm ./MQ
cp ./MQServer/MQSeriesClient*.rpm ./MQ 
cp ./MQServer/MQSeriesSDK*.rpm ./MQ 
cp ./MQServer/MQSeriesSample*.rpm ./MQ 
cp ./MQServer/MQSeriesJava*.rpm ./MQ 
cp ./MQServer/MQSeriesMan*.rpm ./MQ 
cp ./MQServer/MQSeriesJRE*.rpm ./MQ 
cp ./MQServer/MQSeriesGSKit*.rpm ./MQ 
