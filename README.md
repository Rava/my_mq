### Запуск удаленного менеджера очередй WebsphereMQ при помощи docker(использую образ centos:latest). 
#####  Настройка параметров ОС:
``` bash
cd /etc/sysctl.conf 
net.ipv4.tcp_keepalive_time = 300
fs.file-max = 524288
kernel.sem = 500 256000 250 1024
```
#####  Применения изменений ОС:
``` bash 
sysctl -p
```
#####   Также добавим изменения:
``` bash
cd /etc/security/limits.conf 
mqm              hard    nofile          10240
mqm              soft    nofile          10240
mqm              hard    nproc           65535
mqm              soft    nproc           65535
```

##### Создаем рабочую католог и скачиваем бинарный пакет:
```bash
curl -LO https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqadv/mqadv_dev80_linux_x86-64.tar.gz
```

##### Пишем небольшой скрипт для извлечения нужных пакетов для docker 
``` bash 
cat MQ_for_docker.sh 
#!/bin/bash
tar -xzvf mqadv_dev80_linux_x86-64.tar.gz
mkdir -p ./MQ
cp ./MQServer/MQSeriesRuntime*.rpm ./MQ
cp ./MQServer/MQSeriesServer*.rpm ./MQ
cp ./MQServer/MQSeriesClient*.rpm ./MQ 
cp ./MQServer/MQSeriesSDK*.rpm ./MQ 
cp ./MQServer/MQSeriesSample*.rpm ./MQ 
cp ./MQServer/MQSeriesJava*.rpm ./MQ 
cp ./MQServer/MQSeriesMan*.rpm ./MQ 
cp ./MQServer/MQSeriesJRE*.rpm ./MQ 
cp ./MQServer/MQSeriesGSKit*.rpm ./MQ 
```
``` bash
bash MQ_for_docker.sh
ll ./MQ/
итого 166808
-rw-r--r--. 1 spo_04 spo_04  3760173 ноя 24 02:21 MQSeriesClient-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04 24681932 ноя 24 02:21 MQSeriesJava-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04 99374147 ноя 24 02:21 MQSeriesJRE-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04   268452 ноя 24 02:21 MQSeriesMan-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04 22917039 ноя 24 02:21 MQSeriesRuntime-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04   796939 ноя 24 02:21 MQSeriesSamples-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04   297766 ноя 24 02:21 MQSeriesSDK-8.0.0-4.x86_64.rpm
-rw-r--r--. 1 spo_04 spo_04 18700634 ноя 24 02:21 MQSeriesServer-8.0.0-4.x86_64.rpm
```
##### Пишем небольшой скрипт для извлечения нужных пакетов для docker  запустим
```bash
#!/bin/bash
mkdir -p /tmp/mq_license_8.0.0/license/ 
echo -e "#rava\nstatus=9\n"  > /tmp/mq_license_8.0.0/license/status.dat 
mkdir ./MQExplorer
cp ./MQServer/MQSeriesRuntime*.rpm ./MQExplorer
cp ./MQServer/MQSeriesJRE*.rpm ./MQExplorer 
cp ./MQServer/MQSeriesGSKit*.rpm ./MQExplorer
cp ./MQServer/MQSeriesExplorer*.rpm ./MQExplorer
rpm -ivh ./MQExplorer/*.rpm
```


##### Создаем Dockerfile на базе образа Centos:7.5
```bash
FROM centos:latest
# copy inside files in dockerfile
COPY /MQ /opt/
COPY config.mqsc /etc/mqm/
COPY webspheremq.service  /etc/systemd/system/

RUN yum -y update;\ 
    (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
    rm -f /lib/systemd/system/multi-user.target.wants/*;\
    rm -f /etc/systemd/system/*.wants/*;\
    rm -f /lib/systemd/system/local-fs.target.wants/*; \
    rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
    rm -f /lib/sysclstemd/system/basic.target.wants/*;\
    rm -f /lib/systemd/system/anaconda.target.wants/*
RUN mkdir -p /tmp/mq_license_8.0.0/license/ \
    && echo -e "#rava\nstatus=9\n"  > /tmp/mq_license_8.0.0/license/status.dat \
    && cd /opt/ \
    && rpm -ivh MQ*.rpm \
    && rm /opt/*.rpm \
    && chown -R mqm:mqm /opt/mqm/ \
    && chmod +x /opt/mqm/bin/ \
    && /opt/mqm/bin/setmqinst -p /opt/mqm/ -i \
    &&  su mqm -c  "crtmqm QM  && strmqm QM" \
    && echo -e "SSL:\n   AllowWeakCipherSpec=TLS_RSA_WITH_NULL_SHA256" >> /var/mqm/qmgrs/QM/qm.ini \
    && su mqm -c "runmqsc QM  < /etc/mqm/config.mqsc" \
    && systemctl enable /etc/systemd/system/webspheremq.service
VOLUME ["/sys/fs/cgroup"]
EXPOSE 1414
ENV LANG=en_US.UTF-8
CMD ["/usr/sbin/init"]
```
##### Создаем docker volume
```bash
docker volume create --name Datamqm
```

#####  Запуск docker для второго варианта, в нем теперь будет будет работать systemd 
```bash
docker build -t cmq -f Dockerfile .
docker run --name ibm_mq --privileged  -d -e container=docker -v /sys/fs/cgroup:/sys/fs/cgroup  -v Datamqm:/var/mqm -p 1414:1414  cmq
```
##### Cмотрим что контейнер запустился прослушивает порт, и внем работает QM
```bash
 [root@localhost abc_cloud_lab]# docker exec -it ibm_mq /bin/bash
[root@4efb8dad5d18 /]# dspmq
QMNAME(QM)                                                STATUS(Running)
[root@4efb8dad5d18 /]# systemctl status wedspheremq
Unit wedspheremq.service could not be found.
[root@4efb8dad5d18 /]# systemctl status webspheremq
● webspheremq.service - IBM WebSphereMQ
   Loaded: loaded (/etc/systemd/system/webspheremq.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2018-12-23 11:29:37 UTC; 1min 23s ago
  Process: 19 ExecStart=/bin/bash -c /opt/mqm/bin/strmqm QM (code=exited, status=0/SUCCESS)
 Main PID: 23 (amqzxma0)
   CGroup: /docker/4efb8dad5d18c08f5a19889b490ddf076b1dc34e42e7d3202b36ce96d11f8198/system.slice/webspheremq.service
           ├─23 /opt/mqm/bin/amqzxma0 -m QM -u mqm
           ├─28 /opt/mqm/bin/amqzfuma -m QM
           ├─33 /opt/mqm/bin/amqzmuc0 -m QM
           ├─48 /opt/mqm/bin/amqzmur0 -m QM
           ├─53 /opt/mqm/bin/amqzmuf0 -m QM
           ├─57 /opt/mqm/bin/amqrrmfa -m QM -t2332800 -s2592000 -p2592000 -g5184000 -c3600
           ├─70 /opt/mqm/bin/amqzmgr0 -m QM
           ├─75 /opt/mqm/bin/amqfqpub -mQM
           ├─82 /opt/mqm/bin/runmqchi -m QM -q SYSTEM.CHANNEL.INITQ -r
           ├─83 /opt/mqm/bin/amqpcsea QM
           ├─84 /opt/mqm/bin/runmqlsr -r -m QM -t TCP -p 1414
           ├─85 /opt/mqm/bin/amqzlaa0 -mQM -fip0
           └─88 /opt/mqm/bin/amqfcxba -m QM

Dec 23 11:29:35 4efb8dad5d18 systemd[1]: Starting IBM WebSphereMQ...
Dec 23 11:29:36 4efb8dad5d18 bash[19]: WebSphere MQ queue manager 'QM' starting.
Dec 23 11:29:36 4efb8dad5d18 bash[19]: The queue manager is associated with installation 'Installation1'.
Dec 23 11:29:36 4efb8dad5d18 bash[19]: 108 log records accessed on queue manager 'QM' during the log replay phase.
Dec 23 11:29:36 4efb8dad5d18 bash[19]: Log replay for queue manager 'QM' complete.
Dec 23 11:29:37 4efb8dad5d18 bash[19]: 0 log records accessed on queue manager 'QM' during the recovery phase.
Dec 23 11:29:37 4efb8dad5d18 bash[19]: Transaction manager state recovered for queue manager 'QM'.
Dec 23 11:29:37 4efb8dad5d18 bash[19]: WebSphere MQ queue manager 'QM' started using V8.0.0.4.
Dec 23 11:29:37 4efb8dad5d18 systemd[1]: Started IBM WebSphereMQ.
```
